# mini activity
# create a dictionary for an employee containing its name, level and if they are regular

employee_info = {
	"Name" : "Rhett Mclaughlin",
	"Level" : "Co-founder",
	"is_regular" : True
}

information = {
	"Name" : "Link Neal",
	"Age" : 44,
	"is_a_youtuber" : True
}

print(information)
print('')

# using dict function

company_info = dict(
	{
		"Company Name" : "Mythical Entertainment",
		"Nature" : "Media",
		"is_big_company" : True
	}
)

print(company_info)
print('')

# creating nested dictionaries

companies = {
	"Company 1" : company_info,
	"Company 2" : {
		"Company Name" : "Hybe Corporation",
		"Nature" : "Entertainment",
		"is_big_company" : True
	},
	"Company 3" : {
		"Company Name" : "Big Hit Entertainment",
		"Nature" : "Entertainment",
		"is_big_company" : False
	}
}

print(companies)
print('')

# review: dictionary methods
# clear() - clears out the whole dictionary
information.clear()
print(information)
print('')

# copy() - returns a copy of dictionary
copy_employee_info = employee_info.copy()
print(copy_employee_info)
print('')

# difference between copy() and =
example_1 = {"id" : 1, "username" : "thethinker"}
copy_1 = example_1.copy()
copy_1.clear()
print(f"copy method: {copy_1}")
print(f"1: {example_1}")
print('')

example_2 = {"id" : 2, "username" : "domonchi"}
copy_2 = example_2
copy_2.clear()
print(f"assignment operator: {copy_2}")
print(f"2: {example_2}")
print('')

# get() - returns the value for the specified key if the key is in the dictionary
print(copy_employee_info.get("Name"))
print('')

# items() - returns a view object that displays a list of dictionary's tuple pairs
print(copy_employee_info.items())
print('')

# keys() - extracts the keys of the dictionary and returns the list of keys as a view object
print(copy_employee_info.keys())
print('')

# pop(key) - removes and returns an element from a dictionary having the given key
print(copy_employee_info.pop("Name"))
print('')

# popitem() - removes and returns the last element pair inserted into the dictionary
copy_employee_info.popitem()
print(copy_employee_info)
print('')

# update() - updates the dictionary with elements from a dictionary objects
copy_employee_info.update(company_info)
print(copy_employee_info)
print('')

# values() - returns a view object that displays a list of all the values in the dictionary
print(copy_employee_info.values())
print('')

# Hash Table

# accessing values
grocery_prices = {
	"Onion" : 600,
	"Chicken" : 55,
	"Banana" : 90,
	"Pork" : 250
}

print(grocery_prices['Banana'])
print('')

# accessing values using for loops
for item in grocery_prices.items():
	# print(item[0])
	# print(item[1])
	# print(item)
	print('Item: {name}, price: {price}'.format(name = item[0], price = item[1]))

# updating values
grocery_prices["Onion"] = 20
grocery_prices.pop("Pork")
grocery_prices.popitem()
print('')
print(grocery_prices)
print('')

# data frame
import pandas as pd

game_stats = {
	"player_1" : [72, 2, 9, 5],
	"player_2" : [65, 4, 12, 7]
}

df = pd.DataFrame(game_stats)
print(df)
print('')

# iloc[] - locates the specific row you want to display
print("Value of row 1")
print(df.iloc[1])
print('')

# naming your own indexes
df = pd.DataFrame(game_stats, index = ["Level", "K", "D", "A"])
print(df)
print('')

# implementation of dataframe with csv data
df = pd.read_csv("./session_4.csv")
print(df)